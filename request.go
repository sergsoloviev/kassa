package kassa

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/google/uuid"
)

type Request struct {
	Url            string
	Method         string
	Body           []byte
	ResponseBody   []byte
	Client         *http.Client
	Request        *http.Request
	Response       *http.Response
	Timeout        time.Duration
	Authorization  string
	Retry          int
	IdempotenceKey uuid.UUID
	Config         *Config
}

//type Response struct {
//	Error interface{} `json:"error"`
//	Data  interface{} `json:"data"`
//}

func (o *Request) Make() (err error) {
	auth := fmt.Sprintf("%d:%s", o.Config.ShopId, o.Config.Key)
	o.Authorization = base64.StdEncoding.EncodeToString([]byte(auth))

	url := fmt.Sprintf("%s%s", o.Config.APIEndpoint, o.Url)
	if len(o.Body) == 0 {
		o.Request, err = http.NewRequest(o.Method, url, nil)
	} else {
		o.Request, err = http.NewRequest(o.Method, url, bytes.NewBuffer(o.Body))
	}
	if err != nil {
		log.Println(err)
		return err
	}
	o.Request.Header.Set("Content-type", "application/json")
	o.Request.Header.Set("Accept", "application/json")
	if o.IdempotenceKey.String() != "00000000-0000-0000-0000-000000000000" {
		o.Request.Header.Set("Idempotence-Key", o.IdempotenceKey.String())
	}
	//o.Request.Header.Set("Accept-Encoding", "gzip, deflate")
	//o.Request.Header.Set("User-Agent", "HTTPie/0.9.9")
	//o.Request.Header.Set("User-Agent", "curl/7.54.0")
	if o.Authorization != "" {
		o.Request.Header.Set("Authorization", "Basic "+o.Authorization)
	}
	o.Client = &http.Client{}
	o.Response, err = o.Client.Do(o.Request)
	if err != nil {
		log.Println(err)
		return err
	}
	log.Printf("API: %s %d %s", o.Method, o.Response.StatusCode, url)
	o.ResponseBody, err = ioutil.ReadAll(o.Response.Body)
	defer o.Response.Body.Close()
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
