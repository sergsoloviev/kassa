package kassa

import (
	"encoding/json"
	"fmt"
	"log"

	"bitbucket.org/sergsoloviev/kassa/models"
	"github.com/google/uuid"
)

type Config struct {
	APIEndpoint string
	ShopId      int
	Key         string
	Test        bool
}

type Kassa struct {
	Config *Config
}

type APIError struct {
	Type        string `json:"type"`
	Id          string `json:"id"`
	Code        string `json:"code"`
	Description string `json:"description"`
	Parameter   string `json:"parameter"`
}

func (o *Kassa) PaymentGet(id string) (payment *models.Payment, apiError *APIError) {
	req := Request{
		Url:    fmt.Sprintf("%s/%s", "/payments", id),
		Method: "GET",
		Config: o.Config,
	}
	req.Make()
	isError, apiError := o.isAPIError(req.ResponseBody)
	if isError {
		return nil, apiError
	}
	err := json.Unmarshal(req.ResponseBody, &payment)
	if err != nil {
		log.Println(err)
		return nil, nil
	}
	return payment, nil
}

func (o *Kassa) PaymentPost(p *models.Payment) (payment *models.Payment, apiError *APIError) {
	idKey, err := uuid.NewRandom()
	if err != nil {
		log.Println(err)
		return
	}
	payload, err := json.Marshal(p)
	if err != nil {
		log.Println(err)
		return
	}
	req := Request{
		Url:            "/payments",
		Method:         "POST",
		Config:         o.Config,
		IdempotenceKey: idKey,
		Body:           payload,
	}
	req.Make()
	isError, apiError := o.isAPIError(req.ResponseBody)
	if isError {
		return nil, apiError
	}

	err = json.Unmarshal(req.ResponseBody, &payment)
	if err != nil {
		log.Println(err)
		return nil, nil
	}
	return payment, nil
}

func (o *Kassa) PaymentCapture(p *models.Payment) (payment *models.Payment, apiError *APIError) {
	idKey, err := uuid.NewRandom()
	if err != nil {
		log.Println(err)
		return
	}
	url := fmt.Sprintf("/payments/%s/capture", p.Id)
	req := Request{
		Url:            url,
		Method:         "POST",
		Config:         o.Config,
		IdempotenceKey: idKey,
	}
	req.Make()
	isError, apiError := o.isAPIError(req.ResponseBody)
	if isError {
		return nil, apiError
	}

	err = json.Unmarshal(req.ResponseBody, &payment)
	if err != nil {
		log.Println(err)
		return nil, nil
	}
	return payment, nil

}

func (o *Kassa) isAPIError(response []byte) (isErr bool, e *APIError) {
	err := json.Unmarshal(response, &e)
	if err != nil {
		log.Println(err)
		return false, e
	}
	if e.Type != "error" {
		return false, e
	}
	return true, e
}
