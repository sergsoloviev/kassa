package models

type Notification struct {
	Type   string   `json:"type"`
	Event  string   `json:"event"`
	Object *Payment `json:"object"`
}
