package models

type Payment struct {
	Id                   string                 `json:"id,omitempty"`                    // required Идентификатор платежа.
	Status               string                 `json:"status,omitempty"`                // required Статус платежа. Возможные значения: pending, waiting_for_capture, succeeded и canceled. Подробнее про жизненный цикл платежа
	Amount               *Amount                `json:"amount,omitempty"`                // required Сумма платежа. Иногда партнеры Яндекс.Кассы берут с пользователя дополнительную комиссию, которая не входит в эту сумму.
	Receipt              *Receipt               `json:"receipt,omitempty"`               // Данные для формирования чека в онлайн-кассе (для соблюдения 54-ФЗ). Необходимо указать что-то одно — телефон пользователя (phone) или его электронную почту (email).
	PaymentMethod        *PaymentMethod         `json:"payment_method,omitempty"`        // required Способ оплаты, который был использован для этого платежа.
	CreatedAt            string                 `json:"created_at,omitempty"`            // required Время создания заказа. Указывается по UTC и передается в формате ISO 8601. Пример: 2017-11-03T11:52:31.827Z
	Test                 bool                   `json:"test,omitempty"`                  // required Признак тестовой операции.
	Paid                 bool                   `json:"paid,omitempty"`                  // required Признак оплаты заказа.
	Description          string                 `json:"description,omitempty"`           // Описание транзакции, которое вы увидите в личном кабинете Яндекс.Кассы, а пользователь — при оплате. Например: «Оплата заказа № 72 для user@yandex.ru».
	Recipient            *Recipient             `json:"recipient,omitempty"`             // Получатель платежа. Нужен, если вы разделяете потоки платежей в рамках одного аккаунта или создаете платеж в адрес другого аккаунта.
	CapturedAt           string                 `json:"captured_at,omitempty"`           // Время подтверждения платежа. Указывается по UTC и передается в формате ISO 8601.
	ExpiresAt            string                 `json:"expires_at,omitempty"`            // Время, до которого вы можете бесплатно отменить или подтвердить платеж. В указанное время платеж в статусе waiting_for_capture будет автоматически отменен. Указывается по UTC и передается в формате ISO 8601. Пример: 2017-11-03T11:52:31.827Z
	Confirmation         *Confirmation          `json:"confirmation,omitempty"`          // Выбранный способ подтверждения платежа. Присутствует, когда платеж ожидает подтверждения от пользователя. Подробнее про сценарии подтверждения
	RefundedAmount       *RefundedAmount        `json:"refunded_amount,omitempty"`       // Сумма, которая вернулась пользователю. Присутствует, если у этого платежа есть успешные возвраты.
	ReceiptRegistration  string                 `json:"receipt_registration,omitempty"`  // Статус доставки данных для чека в онлайн-кассу (pending, succeeded или canceled). Присутствует, если вы используете решение Яндекс.Кассы для работы по 54-ФЗ.
	MetaData             map[string]interface{} `json:"metadata,omitempty"`              // Любые дополнительные данные, которые нужны вам для работы с платежами (например, номер заказа). Передаются в виде набора пар «ключ-значение» и возвращаются в ответе от Яндекс.Кассы. Ограничения: максимум 16 ключей, имя ключа не больше 32 символов, значение ключа не больше 512 символов.
	CancellationDetails  *CancellationDetails   `json:"cancellation_details,omitempty"`  // Комментарий к статусу canceled: кто отменил платеж и по какой причине. Подробнее про отмененные платежи
	AuthorizationDetails *AuthorizationDetails  `json:"authorization_details,omitempty"` // Данные об авторизации платежа.
}

type Receipt struct {
	Items        []*Item `json:"items"`
	TaxSytemCode int     `json:"tax_system_code"`
	Phone        string  `json:"phone,omitempty"`
	Email        string  `json:"email,omitempty"`
}

type Item struct {
	Description    string  `json:"description"`
	Quantity       int     `json:"quantity"`
	Amount         *Amount `json:"amount"`
	VatCode        int     `json:"vat_code"`
	PaymentSubject string  `json:"payment_subject,omitempty"`
	PaymentMode    string  `json:"payment_mode,omitempty"`
}

type Amount struct {
	Value    string `json:"value,omitempty"`    // required Сумма в выбранной валюте. Выражается в виде строки и пишется через точку, например 10.00. Количество знаков после точки зависит от выбранной валюты.
	Currency string `json:"currency,omitempty"` // required Код валюты в формате ISO-4217. Должен соответствовать валюте вашего аккаунта.
}

type RefundedAmount struct {
	Value    string `json:"value,omitempty"`    // required Сумма в выбранной валюте. Выражается в виде строки и пишется через точку, например 10.00. Количество знаков после точки зависит от выбранной валюты.
	Currency string `json:"currency,omitempty"` // required Код валюты в формате ISO-4217. Должен соответствовать валюте вашего аккаунта.
}

type Recipient struct {
	GatewayId string `json:"gateway_id,omitempty"` // required Идентификатор шлюза. Используется для разделения потоков платежей в рамках одного аккаунта.
}

type PaymentMethod struct {
	Type          string `json:"type,omitempty"`           // required Тип объекта. alfabank apple_pay bank_card cash google_pay installments mobile_balance qiwi sberbank webmoney yandex_money
	Id            string `json:"id,omitempty"`             // required Идентификатор способа оплаты.
	Saved         bool   `json:"saved,omitempty"`          // required С помощью сохраненного способа оплаты можно проводить безакцептные списания.
	Title         string `json:"title,omitempty"`          // Название способа оплаты.
	Login         string `json:"login,omitempty"`          // (alfabank required) Логин пользователя в Альфа-Клике (привязанный телефон или дополнительный логин).
	Phone         string `json:"phone,omitempty"`          // (sberbank required) Телефон пользователя, на который зарегистрирован аккаунт в Сбербанке Онлайн. Необходим для подтверждения оплаты по смс (сценарий подтверждения external). Указывается в формате ITU-T E.164, например 79000000000.
	AccountNumber string `json:"account_number,omitempty"` // (yandex_money required) Номер кошелька в Яндекс.Деньгах, из которого заплатил пользователь.
	Card          Card   `json:"card,omitempty"`           // (bank_card required) Данные банковской карты.
}

type Card struct {
	First6      string `json:"first6,omitempty"`       // required Первые 6 цифр номера карты (BIN).
	Last4       string `json:"last4,omitempty"`        // required Последние 4 цифры номера карты.
	ExpiryYear  string `json:"expiry_year,omitempty"`  // required Срок действия, год, YYYY.
	ExpiryMonth string `json:"expiry_month,omitempty"` // required Срок действия, месяц, MM.
	CardType    string `json:"card_type,omitempty"`    // required Тип банковской карты. Возможные значения: MasterCard (для карт Mastercard и Maestro), Visa (для карт Visa и Visa Electron), Mir, UnionPay, JCB, AmericanExpress, DinersClub и Unknown.
}

type Confirmation struct {
	Type            string `json:"type,omitempty"`             // required Тип объекта external. (external, redirect) Сценарий, при котором необходимо подождать, пока пользователь самостоятельно подтвердит платеж в стороннем сервисе. Например, пользователь подтверждает платеж ответом на смс или в приложении партнера.
	Enforce         bool   `json:"enforce,omitempty"`          // (redirect required) Требование принудительного подтверждения платежа пользователем. Например, требование 3-D Secure при оплате банковской картой (по умолчанию определяется политикой платежной системы).
	ReturnUrl       string `json:"return_url,omitempty"`       // (redirect required) URL, на который вернется пользователь после подтверждения или отмены платежа на веб-странице.
	ConfirmationUrl string `json:"confirmation_url,omitempty"` // (redirect required) URL, на который необходимо перенаправить пользователя для подтверждения оплаты.
}

type CancellationDetails struct {
	Party  string `json:"party,omitempty"`  // required Участник процесса платежа, который принял решение об отмене транзакции. Может принимать значения yandex_checkout, payment_network и merchant. Подробнее про инициаторов отмены платежа https://kassa.yandex.ru/docs/guides/#podtwerzhdenie-i-otmena
	Reason string `json:"reason,omitempty"` // required Причина отмены платежа. Перечень и описание возможных значений https://kassa.yandex.ru/docs/guides/#podtwerzhdenie-i-otmena
}

type AuthorizationDetails struct {
	Rrn      string `json:"rrn,omitempty"`       // Retrieval Reference Number — уникальный номер транзакции, присваеваемый эмитентом. Используется при оплате банковской картой.
	AuthCode string `json:"auth_code,omitempty"` // Код авторизации банковской карты. Выдается эмитентом и подтверждает проведение авторизации.
}
